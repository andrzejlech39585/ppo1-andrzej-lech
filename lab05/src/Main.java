import java.util.ArrayList;
import java.util.Scanner;

final class Library {

    protected ArrayList<Book> books = new ArrayList<Book>();

    public Library addBook(Book book) {
        this.books.add(book);
        return this;
    }

    public ArrayList<Book> getBooks() {
        return this.books;
    }

}

abstract class Book {

    protected String title;

    public Book(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    abstract public String getType();

    static <book> Book AddNewBook(String title, String type) {
        Book book = new Book(title) {
            @Override
            public String getType() {
                return type;
            }
        };
        return book;
    }
}

class Novel extends Book {

    public Novel(String title) {
        super(title);
    }

    public String getType() {
        return "novel";
    }

}

class Comic extends Book {

    public Comic(String title) {
        super(title);
    }

    public String getType() {
        return "comic";
    }

}

class Textbook extends Book {

    public Textbook(String title) {
        super(title);
    }

    public String getType() {
        return "textbook";
    }

}


public class Main {

    public static void main(String[] args) {
        Library library = new Library();

        library.addBook(new Novel("The Vector Prime"));
        library.addBook(new Novel("Star by Star"));
        library.addBook(new Novel("The Unifying Force"));

        library.addBook(new Comic("The Dark Side"));
        library.addBook(new Comic("Aflame"));
        library.addBook(new Comic("Deluge"));

        library.addBook(new Textbook("Textbook of Maths"));
        library.addBook(new Textbook("Textbook of Biology"));
        library.addBook(new Textbook("Textbook of English"));

        Scanner scanner = new Scanner(System.in);
        int chooser = 0;

        while (chooser != 9) {
            System.out.println("\n1 = All of the books\n2 = Novels\n3 = Comics\n4 = Textbooks\n0 = Add new Book\n9= Exit\n");
            chooser = scanner.nextInt();
            switch (chooser) {
                case 1:
                    for (Book book : library.getBooks()) {
                        System.out.println(book.getType() + " || " + book.getTitle());
                    }
                    break;
                case 2:
                    for (Book book : library.getBooks()) {
                        if (book.getType() == "novel") {
                            System.out.println(book.getType() + " || " + book.getTitle());
                        }
                    }
                    break;

                case 3:
                    for (Book book : library.getBooks()) {
                        if (book.getType() == "comic") {
                            System.out.println(book.getType() + " || " + book.getTitle());
                        }
                    }
                    break;

                case 4:
                    for (Book book : library.getBooks()) {
                        if (book.getType() == "textbook") {
                            System.out.println(book.getType() + " || " + book.getTitle());
                        }
                    }
                    break;

                case 0:
                    System.out.println("Type title");
                    String title = scanner.nextLine();

                    System.out.println("Type type");
                    String type = scanner.nextLine();

                    Book book = Book.AddNewBook(title, type);
                    library.addBook(book);
                    break;

                case 9:
                    break;

                default:
                    System.out.println("Wrong choice");
            }
        }
    }
}
