package ParkingLotManager.Entities

import ParkingLotManager.Interfaces.EntityInterface
import ParkingLotManager.Interfaces.PriviligedCarInterface

 open class Car(private val plate: String) : EntityInterface, PriviligedCarInterface{

    override var payment: Float = 3.5F

    override fun isSuper(): Boolean {
        return false
    }

    override fun identify(): String {
        return "Car with plate number " + plate
    }

    override fun canEnter(): Boolean {
        return true
    }
}

