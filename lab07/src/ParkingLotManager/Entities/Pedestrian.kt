package ParkingLotManager.Entities

import ParkingLotManager.Interfaces.EntityInterface

class Pedestrian : EntityInterface {
    override var payment: Float = 0F

    private val pay: Float = 0F

    private var name = ""

    constructor() {}

    constructor(name: String) {
        this.name = name
    }

    override fun identify(): String {
        return if (!name.isEmpty()) name else "Unknown pedestrian"
    }

    override fun canEnter(): Boolean {
        return true
    }

}
