package ParkingLotManager.Entities

class TeacherCar(plate: String) : Car(plate){

    override var payment: Float = 0F

    override fun isSuper(): Boolean {
        return false
    }

}
