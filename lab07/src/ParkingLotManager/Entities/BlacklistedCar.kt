package ParkingLotManager.Entities

class BlacklistedCar(private var plate: String) : Car(plate) {

    override var payment: Float = 0F

    override fun isSuper(): Boolean {
        return false
    }

    override fun canEnter(): Boolean {
        return false
    }

    override fun identify(): String {
        return "Blacklisted car with plate number " + plate
    }
}