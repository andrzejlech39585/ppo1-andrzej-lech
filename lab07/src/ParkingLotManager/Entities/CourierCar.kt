package ParkingLotManager.Entities

class CourierCar(private var plate: String) : Car(plate) {

    override var payment: Float = 0F

    override fun isSuper(): Boolean {
        return true
    }

    override fun identify(): String {
        return "Privilage car with plate number " + plate
    }
}