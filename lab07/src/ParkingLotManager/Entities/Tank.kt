package ParkingLotManager.Entities

import ParkingLotManager.Interfaces.EntityInterface

class Tank(private val plate: String) : EntityInterface {
    override var payment: Float = 0F

    override fun identify(): String {
        return "Tank with plate number " + plate
    }

    override fun canEnter(): Boolean {
        return false
    }

}