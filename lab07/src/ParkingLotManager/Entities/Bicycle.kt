package ParkingLotManager.Entities

import ParkingLotManager.Interfaces.EntityInterface

class Bicycle : EntityInterface{
    override var payment: Float = 0F

    constructor(payment: Float){
        this.payment = payment
    }

    override fun identify(): String {
        return "Unknown bicycle"
    }

    override fun canEnter(): Boolean {
        return false
    }

}
