package ParkingLotManager

import ParkingLotManager.Entities.*
import ParkingLotManager.Interfaces.EntityInterface
import ParkingLotManager.ParkingLot.getNumberOfBicyclePlaces
import java.util.Random
import java.util.ArrayList
import java.util.Collections

object QueueGenerator {
    var random: Random = Random()

    private var BLACKLISTED_CAR_COUNT: Int = random.nextInt(5) + 10
    private var TANKS_COUNT: Int = random.nextInt(1)
    private var PRIVILIGED_CARS_COUNT: Int = random.nextInt(1)
    private var COURIERS_CARS_COUNT: Int = random.nextInt(1) + 2
    private var ANONYMOUS_PEDESTRIANS_COUNT: Int = random.nextInt(20) + 30
    private var PEDESTRIANS_COUNT: Int = random.nextInt(4) + 1
    private var CARS_COUNT: Int = random.nextInt(20) + 10
    private var TEACHER_CARS_COUNT: Int = random.nextInt(4) + 3
    private var BICYCLES_COUNT: Int = random.nextInt(3) + 1


    private val randomPlateNumber: String
        get() {
            val generator = Random()
            return "DLX " + (generator.nextInt(89999) + 10000)
        }

    private val randomName: String
        get() {
            val names = arrayOf("John", "Jack", "James", "George", "Joe", "Jim")
            return names[(Math.random() * names.size).toInt()]
        }

    fun generate(): ArrayList<EntityInterface> {
        val queue = ArrayList<EntityInterface>()

        for (i in 0..BLACKLISTED_CAR_COUNT) {
            queue.add(BlacklistedCar(randomPlateNumber))
        }

        for (i in 0..TANKS_COUNT) {
            queue.add(Tank(randomPlateNumber))
        }

        for (i in 0..PRIVILIGED_CARS_COUNT) {
            queue.add(PriviligedCar(randomPlateNumber))
        }

        for (i in 0..COURIERS_CARS_COUNT) {
            queue.add(CourierCar(randomPlateNumber))
        }

        for (i in 0..ANONYMOUS_PEDESTRIANS_COUNT) {
            queue.add(Pedestrian())
        }

        for (i in 0..PEDESTRIANS_COUNT) {
            queue.add(Pedestrian(randomName))
        }

        for (i in 0..CARS_COUNT) {
            queue.add(Car(randomPlateNumber))
        }

        for (i in 0..TEACHER_CARS_COUNT) {
            queue.add(TeacherCar(randomPlateNumber))
        }

        for (i in 0..BICYCLES_COUNT) {
            if (i < getNumberOfBicyclePlaces()) {
                queue += Bicycle(0F)
            } else {
                queue += Bicycle(0.5F)
            }
        }

        Collections.shuffle(queue)

        return queue
    }

}
