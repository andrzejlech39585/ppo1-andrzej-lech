package ParkingLotManager.Interfaces

interface EntityInterface {
    var payment: Float

    fun identify(): String
    fun canEnter(): Boolean

}
