package ParkingLotManager.Interfaces

interface PriviligedCarInterface {
    fun isSuper(): Boolean
}