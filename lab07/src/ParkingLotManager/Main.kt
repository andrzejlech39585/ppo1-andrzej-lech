package ParkingLotManager

fun main(args: Array<String>) {
    var queue = QueueGenerator.generate()
    var parking = ParkingLot
    val hours: Array<Int> = arrayOf(7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20)

    Log.info("There's " + parking.countCars() + " cars in the parking lot")
    Log.info()



    for (hour in hours) {
        QueueGenerator.generate()
        for (entity in queue) {
            if (parking.checkIfCanEnter(entity)){
                parking.letIn(entity,hour)

            }else{
                Log.info(entity.identify()+" not let in.")
            }
        }
    }
    Log.info()
    Log.info("There's " + parking.countCars() + " cars in the parking lot")
    Log.info("There's " + parking.getMoneyBank() + "$ in the bank")
}