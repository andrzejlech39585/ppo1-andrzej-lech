package ParkingLotManager

import ParkingLotManager.Entities.Car
import ParkingLotManager.Interfaces.EntityInterface
import ParkingLotManager.Interfaces.PriviligedCarInterface
import java.time.LocalTime

import java.util.ArrayList

object ParkingLot {

    private var moneyBank: Float = 0F
    private val entitiesOnProperty = ArrayList<EntityInterface>()
    private var carsOnProperty = 0
    private var numberOfPlaces = 300
    private val numberOfBicyclePlaces = 7

    fun getNumberOfBicyclePlaces(): Int{
        return numberOfBicyclePlaces
    }

    fun getTime(hour: Int): String {
        var i: Int = LocalTime.now().minute
        if (i == 60) {
            i = 0
        }
        return "At " + hour + ":" + i + " "
        i++
    }

    fun getMoneyBank(): Float {
        return moneyBank
    }

    fun checkIfCanEnter(entity: EntityInterface): Boolean {
        return entity.canEnter()
    }

    fun letIn(entity: EntityInterface, hour: Int) {

        moneyBank += entity.payment


        if (entity is Car) {
            if (entity.isSuper()) {
                Log.info(getTime(hour) + entity.identify() + " let in.")
            } else {
                if (carsOnProperty >= numberOfPlaces) {
                    Log.info("Parking lot is full. " + entity.identify() + " not let in.")
                } else {
                    entitiesOnProperty.add(entity)
                    Log.info(getTime(hour) + entity.identify() + " let in.")
                    carsOnProperty++
                }
            }
        } else {
            Log.info(getTime(hour) + entity.identify() + " let in.")
        }
    }

    fun countCars(): Int {
        return carsOnProperty
    }
}