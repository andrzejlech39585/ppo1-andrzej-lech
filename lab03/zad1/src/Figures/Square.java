package Figures;

class Square {
    public int n = 4;
    private Point[] points = new Point[4];
    private float length = 0;

    public Square() {
        Point firstPoint = new Point(Main.getRandomNumber(), Main.getRandomNumber());

        do {
            length = (float) Main.getRandomNumber();
        } while (length <= 0);


        points[0] = (firstPoint);

        points[1].movePoint(length, 0);

        firstPoint.movePoint(0, length);
        points[2] = firstPoint;


        firstPoint.movePoint((-1 * length), 0);
        points[3] = firstPoint;

        System.out.println("Created a square");
    }

    public void printSquare() {
        for (int i = 0; i < n; i++) {
            points[i].printPoint();
        }
    }
}