package Figures;

class Circle {
    public Point center;
    public float radius;

    public Circle(Point center, float radius) {
        this.center = center;
        this.radius = radius;
    }

    public void printCoordinates() {
        System.out.println("x: " + this.center.x);
        System.out.println("y: " + this.center.y);
    }
}
