package Figures;

import java.util.ArrayList;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        float inputX = 0;
        float inputY = 0;
        float inputRadius = 5;

        int l = 1 - 1;

        ArrayList<Square> squares = new ArrayList<Square>();

        for (int i = 0; i < l + 1; i++) {
            Square square = new Square();
            squares.add(square);
        }

        for (int i = 0; i < l + 1; i++) {
            System.out.println("Figures.Square " + (i + 1) + ": ");
            squares.get(i).printSquare();
        }

        Point point = new Point(inputX, inputY);
        Circle circle = new Circle(point, inputRadius);

        circle.center.movePoint(Main.getRandomNumber(), Main.getRandomNumber());
        circle.printCoordinates();
    }

    public static float getRandomNumber() {
        Random rand = new Random();
        return rand.nextInt(10);
    }
}