package Figures;

class Point {

    public float x;
    public float y;

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
        System.out.println("Figures.Point [" + x + ", " + y + "] has been created.");
    }

    public void movePoint(float xAxisShift, float yAxisShift) {
        this.x += xAxisShift;
        this.y += yAxisShift;
    }

    public void printPoint() {
        System.out.println("[" + x + ", " + y + "]");
    }
}