﻿using System;

namespace lab08
{
    class Program
    {
        static void Main(string[] args)
        {
            Library library = new Library("Biblioteka nr 1");
            
            Section section1 = new Section("section1");
            library += section1;

            Section section2 = new Section("section2");
            library += section2;

            Section section3 = new Section("section3");
            library += section3;

            Section.generateSection(section1);
            Section.generateSection(section2);
            Section.generateSection(section3);

            library.Print();

            library -= section1;

            library.Print();

            Console.ReadKey();
        }
    }
}
