﻿using System;
using System.Collections.Generic;

namespace lab08
{
   internal class Section
    {
        public string Name { get; set; }
        public List<Book> Books { get; set; } = new List<Book>();

        public Section(string name)
        {
            Name = name;
        }

        public static Section operator +(Section section, Book book)
        {
            section.Books.Add(book);
            return section;
        }

        internal static Section generateSection(Section section) {
            int i;

            for (i = 0; i <= 10; i++)
            {
                Book book = new Book("Book"+i, "Autor"+i);
                section += book;
            }
            return section;
        }

        public void Print()
        {
            Console.WriteLine(Name);
            foreach (var book in Books)
                book.Print();
        }
    }
}