﻿using System;
using System.Collections.Generic;

namespace lab08
{
    class Library
    {
        public string Name { get; set; }
        public List<Section> Sections { get; set; } = new List<Section>();

        public Library(string name)
        {
            Name = name;
        }

        public static Library operator +(Library library,Section section)
        {
            library.Sections.Add(section);
            return library;
        }

        public static Section operator +(Section section, List<Book> Books)       
        {
            for (int i = 0; i <Books.Count; i++) {
                Book book = Books[i];
                section.Books.Add(book);
            }
            return section;
        }

        public static Library operator -(Library library, Section section)
        {
            for(int i = 0; i < library.Sections.Count; i++)
            {
                if (section == library.Sections[i])
                {
                    library.Sections[i] = null;
                }
            }

            return library;
        }

        public void Print()
        {
            Console.WriteLine(Name);
            foreach (var section in Sections)
                section.Print();
        }
    }

   
}
