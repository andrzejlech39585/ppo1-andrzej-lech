import random
from force_users import *

n = 25
champions = []
names = [
"Mark Clifford",
"Kiyan Field",
"Robert Waters",
"Fynley Carr",
"Theodor Chen",
"Xavier Justice",
"Grayson Roberson",
"Adil Haines",
"Chris Portillo",
"Safaa Cohen",
"Wilbur Hull",
"Keiron Whitley",
"Jaya Cano",
"Nana Henderson",
"Elsie-Mae Yang",
"Dilara Pham",
"Aleyna Conroy",
"Keira Salas",
"Beth Farrington",
"Ava-Mai Fitzgerald",
"Antonio Leach",
"Madelaine Brewer",
"Vinny Thomas",
"Dolcie Blundell",
"Krish Bradshaw",
]

for i in range(n):

   rand = random.randint(2,6)
   if(rand==2):
     champions.append(LightsaberUser(random.choice(names)))
   if(rand==3):
     champions.append(LightSideForceUser(random.choice(names)))
   if(rand==4):
     champions.append(DarkSideForceUser(random.choice(names)))
   if(rand==5):
     champions.append(SithLord(random.choice(names)))
   if(rand==6):
     champions.append(JediMaster(random.choice(names)))

for champ in champions:
  print(champ.name)

dead = []

while len(champions) > 1:
    attacker = random.choice(champions)
    target = attacker

    while attacker == target:
        target = random.choice(champions)

    attacker.attack(target)
    if target.life_points <= 0:
        print(target.name + " died.")
        dead.append(target.name)
        champions.remove(target)

print("")

for name in dead:
  print(name + " died")


print("\nThe winner is " + champions[0].name)