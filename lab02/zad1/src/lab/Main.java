package lab;

import java.util.ArrayList;
import java.util.Random;

class Student {

    public String studentNo;
    public String studentName;
    public String studentLastName;
    public int studentStatus;


    public void setStudentStatus(int studentStatus) {
        this.studentStatus = studentStatus;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public void setStudentLastName(String studentLastName) {
        this.studentLastName = studentLastName;
    }

    public void setStudentNo(String studentNo) {
        this.studentNo = studentNo;
    }

    public int getStudentStatus() {
        return this.studentStatus;
    }

    public String getStudentLastName() {
        return this.studentLastName;
    }

    public String getStudentNo() {
        return this.studentNo;
    }

    public String getStudentName() {
        return this.studentName;
    }
}

public class Main {

    static String namesBase[] = {"Andrzej", "Mateusz", "Zofia", "Emilia", "Kasper", "Kasia", "Dominik", "Kamila", "Krzysztof", "Damian", "Anna", "Marta"};
    static String lastNameBase[] = {"Bereć", "Leśniak", "Lech", "Berć", "Kowal", "Sikora", "Bhor", "Planck", "Shannon", "Armstrong", "Zawala", "Domagała", "Dąb"};
    static int students_count = 10;

    public static void main(String[] args) {
        ArrayList<Student> students = new ArrayList<Student>();

        for (int i = 0; i < students_count; i++) {
            Student student = new Student();
            student.setStudentLastName(getRandomStudentLastName());
            student.setStudentName(getRandomStudentName());
            student.setStudentNo(getRandomStudentNumber());
            student.setStudentStatus(getRandomStudentStatus());
            students.add(student);
        }

        System.out.println("Students group have been filled.");

        for (int i = 0; i < students.size(); i++) {
            Student student = students.get(i);
            if (student.getStudentStatus() == 1) {
                System.out.println(student.getStudentLastName() + " " + student.getStudentName() + " (" + student.getStudentNo() + ")");
            }
        }
    }

    protected static String getRandomStudentNumber() {
        Random rand = new Random();
        return String.valueOf(rand.nextInt(2000) + 38000);
    }

    protected static String getRandomStudentName() {
        Random rand = new Random();
        return namesBase[rand.nextInt(namesBase.length)];
    }

    protected static String getRandomStudentLastName() {
        Random rand = new Random();
        return lastNameBase[rand.nextInt(lastNameBase.length)];
    }

    protected static int getRandomStudentStatus() {
        Random rand = new Random();
        return rand.nextInt() % 2;
    }
}


