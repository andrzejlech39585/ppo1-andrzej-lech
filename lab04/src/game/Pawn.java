package game;

import java.util.Random;

class Pawn {
    private int position;
    private String name;

    public Pawn() {
        this.position = 0;
        this.name = generateName();
        System.out.println(this.name + " joined the game.");
    }

    String[] players = {"Luke Skywalker", "Darth Vader", "Yoda", "Obi-wan Kenobi", "Jar Jar Binks", "C-3PO", "Chewbacca", "A2-D2", "Stormtrooper", "Jabba"};

    int getPosition() {
        return position;
    }

     void setPosition(int position) {
        this.position = position;
    }

    String getName() {
        return name;
    }


     String generateName() {
        Random rand = new Random();
        return players[rand.nextInt(players.length)];
    }

}