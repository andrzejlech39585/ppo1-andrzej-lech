package game;

import java.util.Random;

class Board {

    private int MAX_POSITION;
    Random rand = new Random();

    static int generateWinCondition() {
        Random rand = new Random();
        int winCodndition = (rand.nextInt(4) + 1) * 50;
        System.out.println("Win condition is: " + winCodndition);
        return winCodndition;
    }


    int numberOfPawns() {
        int number = rand.nextInt(7) + 3;
        return number;
    }

    private int numberOfPawns = numberOfPawns();
    private Pawn[] pawns = new Pawn[numberOfPawns];
    private Dice dice;
    private Pawn winner;
    private int turnsCounter;
    private Board board;
    private int MAX_TURN;

    int i = 0;

    Board(Dice dice, int MAX_POSITION, int MAX_TURN) {
        this.numberOfPawns = numberOfPawns();
        this.board = board;
        this.dice = dice;
        this.winner = null;
        this.turnsCounter = 0;
        this.MAX_POSITION = MAX_POSITION;
        this.MAX_TURN = MAX_TURN;
        System.out.println("Max turn is: "+MAX_TURN);
    }

    Board(Dice dice, int MAX_POSITION) {
        this.numberOfPawns = numberOfPawns();
        this.board = board;
        this.dice = dice;
        this.winner = null;
        this.turnsCounter = 0;
        this.MAX_POSITION = MAX_POSITION;
    }

    int getNumberOfPawns() {
        return pawns.length;
    }

    String getWinnersName() {
        return winner.getName();
    }

    public Pawn[] getPawns() {
        return pawns;
    }

    void performTurn() throws Main.WinnerWasCalled {

        this.turnsCounter++;
        System.out.println("Turn " + this.turnsCounter);

        for (Pawn pawn : pawns) {

            int rollResult = this.dice.roll();

            if (rollResult == 1 && pawn.getPosition() % 2 != 0) {
                rollResult = this.dice.roll() * -1;
            }

            if (rollResult == Dice.getSides() && pawn.getPosition() % 10 == 0) {
                rollResult += this.dice.roll();
            }

            System.out.println("Move: " + rollResult);

            pawn.setPosition(pawn.getPosition() + rollResult);
            System.out.println(pawn.getName() + " new position: " + pawn.getPosition());

            if (pawn.getPosition() >= MAX_POSITION || MAX_TURN -1 == turnsCounter) {
                int max = 0;

                for (int i = 0; i < pawns.length; i++) {
                    if (max < pawns[i].getPosition()) {
                        max = pawns[i].getPosition();
                    }
                }

                for (int i = 0; i < pawns.length; i++) {
                    if (max == pawns[i].getPosition())
                        this.winner = pawns[i];
                }
                throw new Main.WinnerWasCalled();
            }

        }
    }
}

