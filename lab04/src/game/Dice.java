package game;

import java.util.Random;

public class Dice {
    private static int sides;


    Dice(int sides) {
    this.sides = sides;
    }

    public static int getSides() {
        return sides;
    }

    static int generateDice() {

        int typesOfDice[] = {4, 6, 8, 10, 12, 20};
        Random rand = new Random();

        int typeOfDice = typesOfDice[rand.nextInt(typesOfDice.length)];
        System.out.println("Type of dice: k" + typeOfDice);
        return typeOfDice;
    }

    int roll() {
        Random rand = new Random();
        int result = rand.nextInt(sides) + 1;
        System.out.println("Dice roll: " + result);
        return result;
    }

}
