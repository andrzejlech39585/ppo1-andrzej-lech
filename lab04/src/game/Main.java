package game;

public class Main {

    static class WinnerWasCalled extends Exception {
    }


    public static void main(String[] args) {
        int sides = Dice.generateDice();
        int maxPosition = Board.generateWinCondition();

        Dice dice= new Dice(sides);
        Board board = new Board(dice,maxPosition,10);
        //Board board = new Board(dice,maxPosition);
        for (int i = 0; i < board.getNumberOfPawns(); i++) {
            board.getPawns()[i] = new Pawn();
        }

        try {
            while (true) {
                board.performTurn();
            }
        } catch (WinnerWasCalled $exception) {
            System.out.println(board.getWinnersName() + " won.");
        }

    }
}